let mouseOffset;
let isMouseDown;
let lastActiveNote = null;
// call back functions for drag and drop
function onMouseDown(event, note) {
    lastActiveNote = note;
    isMouseDown = true;

    mouseOffset = { x: note.offsetLeft - event.clientX, y: note.offsetTop - event.clientY };
    note.style.zIndex = '99999';

}

function onMouseMove(event, note, parent) {
    if (lastActiveNote == null)
        return;
    event.preventDefault();
    let top = parseInt(lastActiveNote.style.top.replace("px", ''));
    let left = parseInt(lastActiveNote.style.left.replace("px", ''));
    let width = parseInt(lastActiveNote.style.width.replace("px", ''));
    let height = parseInt(lastActiveNote.style.height.replace("px", ''));
    let x1 = left + width;
    let x2 = left + width - 150;
    let y1 = top + height + 66;
    let y2 = top + height + 10;
    if (isMouseDown) {
        if (event.clientX >= x2 && event.clientX <= x1 && event.clientY <= y1 && event.clientY >= y2)
            return;
        // set the note positon acording to the mouseoffset
        if (event.clientY + mouseOffset.y < 0)
            lastActiveNote.style.top = 0 + "px";
        else if (event.clientY + mouseOffset.y > window.innerHeight - height - 66)
            lastActiveNote.style.top = (window.innerHeight - height - 66) + "px";
        else
            lastActiveNote.style.top = event.clientY + mouseOffset.y + "px";

        if (event.clientX + mouseOffset.x < 0)
            lastActiveNote.style.left = 0 + "px";

        else if (event.clientX + mouseOffset.x > window.innerWidth - width)
            lastActiveNote.style.left = (window.innerWidth - width) + "px";
        else
            lastActiveNote.style.left = event.clientX + mouseOffset.x + "px";


    }


}
let timecounter = 1;

function onMouseUp(event, note) {
    isMouseDown = false;
    let boardIndex = objectBoard.boardList.findIndex((board) => board.boardNo == boardActiveId);
    let noteIndex = objectBoard.boardList[boardIndex].boardNotes.findIndex((item) => item.id == note.id);
    objectBoard.boardList[boardIndex].boardNotes[noteIndex].setPosition_x(note.style.left);
    objectBoard.boardList[boardIndex].boardNotes[noteIndex].setPosition_y(note.style.top);
    StoreBoard(boardActiveId);
    note.style.zIndex = '' + (timecounter++);
}