class Board {

    constructor(boardNo, boardName) {
        this.boardNo = boardNo;
        this.boardName = boardName;
        this.noteCount = 0;
        this.boardNotes = new Array();
        this.creationData = new Date();

    }

    getBoardNumber() {
        return this.boardNo;
    }

    getBoardName() {
        return this.boardName;
    }

    getNoteCount() {
        return this.noteCount;
    }

    getBoardNotes() {
        return this.boardNotes;
    }

    setBoardNumber(boardNo) {
        this.boardNo = boardNo;

    }

    setBoardName(boardName) {
        this.boardName = boardName;

    }

    setNoteCount(noteCount) {
        this.noteCount = noteCount;

    }

    setBoardNotes(boardNotes) {
        this.boardNotes = boardNotes;

    }

    increaseCount() {
        this.noteCount += 1;
    }
    addNote(note) {
        this.boardNotes.push(note);
        this.increaseCount();
    }
    setCreationDate(creationDate) {
        this.creationData = creationDate;
    }
    getCreationDate() {
        return this.creationData;
    }
}