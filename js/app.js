let objectBoard = {
    boardList: [],
    boardCount: 0
}

let boardArrayId = new Array();
let boardActiveId = -1;

// reterive the previous boards ,notes

document.addEventListener("DOMContentLoaded", function(e) {
    loadBoards();
});

function createNoteDOM(body, color, width, height, top, left) {
    let noteWrapper = document.createElement("div");

    let noteBodyWrapper = document.createElement("div");
    noteBodyWrapper.contentEditable = 'false';
    noteBodyWrapper.appendChild(document.createTextNode(body));

    let noteButtonWrapper = document.createElement("div");
    let noteButtonColorWrapper = document.createElement("div");

    // create draggable note
    noteWrapper.draggable = true;
    noteWrapper.style.position = 'absolute';

    let colorGrewDiv = document.createElement("div");
    let colorRedDiv = document.createElement("div");
    let colorGreenDiv = document.createElement("div");
    let colorBlueDiv = document.createElement("div");

    let deleteButtonDiv = document.createElement("div");

    // append classes to buttons
    deleteButtonDiv.className = 'note-delete circle dark-red-color';
    colorBlueDiv.className = 'circle blue-color';
    colorGreenDiv.className = 'circle green-color';
    colorRedDiv.className = 'circle red-color';
    colorGrewDiv.className = 'circle grew-color';

    // append classes to Wrappers
    noteButtonColorWrapper.className = 'note-colors flex-container justify-content-space-between';
    noteButtonWrapper.className = 'note-buttons flex-container justify-content-space-between';
    noteBodyWrapper.className = 'body flex-grow-1';
    noteWrapper.className = `note flex-container flex-column align-items-end  ${color}`;

    noteButtonColorWrapper.appendChild(colorGrewDiv);
    noteButtonColorWrapper.appendChild(colorRedDiv);
    noteButtonColorWrapper.appendChild(colorGreenDiv);
    noteButtonColorWrapper.appendChild(colorBlueDiv);
    noteButtonWrapper.appendChild(noteButtonColorWrapper);
    noteButtonWrapper.appendChild(deleteButtonDiv);
    noteWrapper.appendChild(noteBodyWrapper);
    noteWrapper.appendChild(noteButtonWrapper);

    // events
    deleteButtonDiv.addEventListener('click', (e) => {
        removeNote(noteWrapper, noteWrapper.id);
    })
    colorGrewDiv.addEventListener('click', (e) => {
        updateNoteColor(noteWrapper, 'grew-color', noteWrapper.id);
    })
    colorRedDiv.addEventListener('click', (e) => {
        updateNoteColor(noteWrapper, 'red-color', noteWrapper.id);
    })
    colorGreenDiv.addEventListener('click', (e) => {
        updateNoteColor(noteWrapper, 'green-color', noteWrapper.id);
    })
    colorBlueDiv.addEventListener('click', (e) => {
        updateNoteColor(noteWrapper, 'blue-color', noteWrapper.id);
    })

    // actionListeners to handle drag and drop event
    noteWrapper.addEventListener("mousedown", (event) => {
        onMouseDown(event, noteWrapper);
    })
    noteWrapper.addEventListener("mouseup", (event) => {
        onMouseUp(event, noteWrapper);
    });

    // note is needed
    noteWrapper.addEventListener("mousemove", (event) => {
        onMouseMove(event, noteWrapper);
    });

    noteBodyWrapper.addEventListener('blur', function(e) {
        finishUpdateElementName(noteBodyWrapper);
        updateBody(noteWrapper.id, e.target.innerText);
        StoreBoard(boardActiveId);
    })

    noteBodyWrapper.addEventListener('dblclick', function(e) {
        updateElementName(noteBodyWrapper);
    })

    // Default value
    noteWrapper.style.width = width;
    noteWrapper.style.height = height;
    noteWrapper.style.top = top;
    noteWrapper.style.left = left;

    var observer = new MutationObserver(function(mutations) {
        let noteWrap = noteWrapper;
        let boardIndex = objectBoard.boardList.findIndex((board) => board.boardNo == boardActiveId);
        let noteIndex = objectBoard.boardList[boardIndex].boardNotes.findIndex((note) => noteWrapper.id == note.id);
        try {
            objectBoard.boardList[boardIndex].boardNotes[noteIndex].height = noteWrap.style.height;
            objectBoard.boardList[boardIndex].boardNotes[noteIndex].width = noteWrap.style.width;
            StoreBoard(boardActiveId);
        } catch (e) {
            console.log("Board is opened");
        }

    });
    observer.observe(noteWrapper, {
        attributes: true
    });

    return noteWrapper;
}

function createNoteObject() {
    let board = objectBoard.boardList.find((board) => board.boardNo == boardActiveId);
    let count = board.getNoteCount();
    let newNote = new Note('new Note' + count, 'grew-color', count, 0, 0, 250, 200);
    board.addNote(newNote);
    return newNote;
}

function addNewNote() {
    let colors = ['grew-color', 'red-color', 'green-color', 'blue-color'];
    if (boardActiveId == -1)
        return;
    let newNote = createNoteObject();
    let newNoteDOM = createNoteDOM("new Note", colors[(Math.floor(Math.random() * 10)) % colors.length], "250px", "200px", 0, 0);
    newNoteDOM.id = newNote.id;
    Array.from(document.getElementById("boards").children).find((board) => board.id == boardActiveId).appendChild(newNoteDOM);
    StoreBoard(boardActiveId);
    return true;
}

function updateNoteElementColor(e, color) {
    e.className = e.className.replace('grew-color', '');
    e.className = e.className.replace('red-color', '');
    e.className = e.className.replace('green-color', '');
    e.className = e.className.replace('blue-color', '');
    e.classList.add(color);
}

function updateNoteColor(e, color, id) {
    updateNoteElementColor(e, color);
    updateColorAttribute(boardActiveId, id, color);
    StoreBoard(boardActiveId);
}

function updateBody(id, noteBody) {
    let boardIndex = objectBoard.boardList.findIndex((item) => item.boardNo == boardActiveId);
    let noteIndex = objectBoard.boardList[boardIndex].boardNotes.findIndex((note) => id == note.id);
    objectBoard.boardList[boardIndex].boardNotes[noteIndex].setNoteBody(noteBody);
    StoreBoard(boardActiveId);
    // save
}

function deleteNoteElement(e) {
    e.parentElement.removeChild(e);
}

function deleteNote(boardId, id) {
    let boardIndex = objectBoard.boardList.findIndex((item) => item.boardNo == boardId);
    let noteIndex = objectBoard.boardList[boardIndex].boardNotes.findIndex((note) => note.id == id)
    objectBoard.boardList[boardIndex].boardNotes.splice(noteIndex, 1);
}

function removeNote(e, id) {
    let boardId = boardActiveId;
    deleteNoteElement(e);
    deleteNote(boardId, id);
    StoreBoard(boardId);
}


function createNoteObject() {
    let board = objectBoard.boardList.find((board) => board.boardNo == boardActiveId);
    let count = board.getNoteCount();
    let newNote = new Note('new Note' + count, 'grew-color', count, 0, 0, 250, 200);
    board.addNote(newNote);
    return newNote;
}

function addNewNote() {
    let colors = ['grew-color', 'red-color', 'green-color', 'blue-color'];
    let colorRandom = colors[(Math.floor(Math.random() * 10)) % colors.length];
    if (boardActiveId == -1)
        return;

    let newNote = createNoteObject();
    let newNoteDOM = createNoteDOM("new Note", colorRandom, "250px", "200px", 0, 0);
    newNoteDOM.id = newNote.id;
    newNote.setColor(colorRandom);
    Array.from(document.getElementById("boards").children).find((board) => board.id == boardActiveId).appendChild(newNoteDOM);
    StoreBoard(boardActiveId);
    return true;
}


function createBoardElementDOM(boardName) {
    let divforTab = document.createElement('div');
    let TabButton = document.createElement('div');

    let xicon = document.createElement('div');
    xicon.className = "circle-small  note-delete-small ";

    divforTab.className = 'board-tab tab-holder';
    TabButton.contentEditable = 'false';
    TabButton.innerHTML = boardName;

    let noteWrapper = document.createElement('div');
    noteWrapper.className = 'note-wrapper';

    // make the board element contains draggable notes
    xicon.addEventListener("click", function(e) {
        deleteBoard(e.target.parentElement, noteWrapper, TabButton.id);
    })
    noteWrapper.style.position = 'absolute';
    TabButton.addEventListener('click', function(e) {
        updateBoardState(divforTab, noteWrapper, xicon);
    });

    TabButton.addEventListener('blur', function(e) {
        finishUpdateElementName(e.target);
        if (e.target.innerText.trim().length == 0)
            e.target.innerText = 'Nameless Board :)';
        e.target.innerHTML = e.target.innerHTML.replace("<br>", " ");
        e.target.innerHTML = e.target.innerHTML.replace("\n", " ");

        updateBoardName(TabButton.id, e.target.innerText);
        StoreBoard(boardActiveId);
    });
    TabButton.addEventListener('dblclick', function(e) {
        updateElementName(TabButton);
    })

    noteWrapper.addEventListener("mousemove", (event) => {
        onMouseMove(event, null, noteWrapper);
    });

    TabButton.addEventListener('keydown', (evt) => {
        if (evt.keyCode === 13) {
            evt.preventDefault();
        }
    });


    divforTab.appendChild(TabButton);
    divforTab.appendChild(xicon);

    return { noteWrapper, TabButton, divforTab };
}

function createNewBoard(boardName) {
    objectBoard.boardCount += 1;
    let boardComponent = createBoardElementDOM(boardName);
    boardComponent.noteWrapper.id = objectBoard.boardCount;
    boardComponent.TabButton.id = objectBoard.boardCount;
    boardComponent.divforTab.id = objectBoard.boardCount;


    let board = new Board(objectBoard.boardCount, boardName);
    objectBoard.boardList.push(board);
    document.getElementById('boards').appendChild(boardComponent.noteWrapper);
    document.getElementById('tabs').appendChild(boardComponent.divforTab);

    boardArrayId.push(objectBoard.boardCount);
    localStorage.setItem("boardsId", JSON.stringify(boardArrayId));
    localStorage.setItem(objectBoard.boardCount, JSON.stringify(board));


    if (objectBoard.boardList.length == 1) {
        boardComponent.divforTab.className += " active";
        boardComponent.noteWrapper.className += " active";
        boardComponent.divforTab.children[1].className += " active";
        boardActiveId = 1;
    }

    return { board, boardComponent };
}

function updateBoardState(tab, noteWrapper, xicon) {
    if (boardActiveId != -1) {
        let tabs = Array.from(document.getElementById('tabs').children);
        let activatedTab = tabs.find((item) => item.id == boardActiveId);


        activatedTab.className = activatedTab.className.replace(" active", "");
        activatedTab.children[1].className = activatedTab.children[1].className.replace(" active", "");

        let notesWrapper = Array.from(document.getElementById('boards').children);
        let wrapper = notesWrapper.find((item) => item.id == boardActiveId);

        wrapper.className = wrapper.className.replace(" active", "");
    }

    tab.className += " active";
    noteWrapper.className += " active";
    xicon.className += " active";
    boardActiveId = tab.id;
}

function updateBoardName(id, boardName) {
    let getIndex = objectBoard.boardList.findIndex((item) => item.boardNo == id);
    boardName = boardName.replace("<br>", " ");
    objectBoard.boardList[getIndex].setBoardName(boardName);

    StoreBoard(getIndex);
    // save
}

function deleteBoardObject(boardId) {
    let boardIndex = objectBoard.boardList.findIndex((board) => board.id == boardId);
    objectBoard.boardList.splice(boardIndex, 1);
}

function deleteBoardDOM(e, wrapper) {
    e.parentElement.removeChild(e);
    wrapper.parentElement.removeChild(wrapper);
}

function deleteBoard(e, wrapper, boardId) {
    deleteBoardDOM(e, wrapper);
    deleteBoardObject(boardId);

    if (boardId == boardActiveId) {
        boardActiveId = -1;
        if (objectBoard.boardList.length) {
            let newIndex = objectBoard.boardList[0].getBoardNumber();

            let tabs = Array.from(document.getElementById('tabs').children);
            let notesWrapper = Array.from(document.getElementById('boards').children);
            updateBoardState(tabs[0], notesWrapper[0], tabs[0].children[1]);
            boardActiveId = notesWrapper[0].id;
        }

    }
    let key = boardArrayId.findIndex((id) => id == boardId);
    boardArrayId.splice(key, 1);

    localStorage.setItem(boardId, null);
    localStorage.setItem("boardsId", JSON.stringify(boardArrayId));
}

function updateColorAttribute(boardId, id, color) {
    let boardIndex = objectBoard.boardList.findIndex((item) => item.boardNo == boardId);
    let noteIndex = objectBoard.boardList[boardIndex].boardNotes.findIndex((note) => note.id == id)
    objectBoard.boardList[boardIndex].boardNotes[noteIndex].setColor(color);
}

function updateElementName(element) {
    if (element.contentEditable == 'false') {
        element.contentEditable = 'true';
        element.className += " update-color";
    }
}

function finishUpdateElementName(element) {
    if (element.contentEditable == 'true') {
        element.contentEditable = 'false';
        element.className = element.className.replace('update-color', "");
    }
}


// call when add, modify note or board
function StoreBoard(boardId) {
    // get the bord content
    let board = objectBoard.boardList.find((board) => boardId == board.boardNo);
    localStorage.setItem(boardId, JSON.stringify(board));
}

// call when load document
function loadBoards() {
    // localStorage.clear();
    let boardData;

    // Load boards Id Array
    let tempBoardIds = JSON.parse(localStorage.getItem("boardsId"));
    if (tempBoardIds != undefined)
        for (let i = 0; i < tempBoardIds.length; i++) {
            boardId = tempBoardIds[i];
            boardData = JSON.parse(localStorage.getItem(boardId));

            //Create Board Object and set its attributes.
            let { board, boardComponent, divforTab } = createNewBoard(boardData.boardName);
            board.setBoardNumber(objectBoard.boardCount);
            board.setCreationDate(objectBoard.creationData);
            //Create Board HTML ELEMENT FOR the board
            //Set the same Id
            boardComponent.noteWrapper.id = objectBoard.boardCount;
            boardComponent.TabButton.id = objectBoard.boardCount;
            boardComponent.divforTab.id = objectBoard.boardCount;
            //get the notes 
            let notes = boardData.boardNotes;
            for (let i = 0; i < notes.length; i++) {
                //Create note DOM
                let Newnote = createNoteDOM(notes[i].noteBody, notes[i].color, notes[i].width, notes[i].height, notes[i].position_y, notes[i].position_x);
                let noteObject = new Note(notes[i].noteBody, notes[i].color, notes[i].id, notes[i].position_x, notes[i].position_y, notes[i].width, notes[i].height);
                boardComponent.noteWrapper.appendChild(Newnote);

                //Assign the note to its board
                board.addNote(noteObject);
                Newnote.id = noteObject.getId();

            }
            localStorage.setItem(objectBoard.boardCount, JSON.stringify(board));
        }
    localStorage.setItem("boardsId", JSON.stringify(boardArrayId));
}