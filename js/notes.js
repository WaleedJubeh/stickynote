class Note {

    constructor(noteBody, color, id, position_x, position_y, width, height) {
        this.noteBody = noteBody;
        this.id = id;
        this.color = color;
        this.position_x = position_x;
        this.position_y = position_y;
        this.width = width;
        this.height = height;
    }

    getNoteBody() {
        return this.noteBody;
    }

    getId() {
        return this.id;
    }

    getColor() {
        return this.color;
    }

    getPosition_x() {
        return this.position_x;
    }

    getPosition_y() {
        return this.position_y;
    }

    getWidth() {
        return this.width;
    }
    getHeight() {
        return this.height;
    }

    setNoteBody(noteBody) {
        this.noteBody = noteBody;

    }

    setId(id) {
        this.id = id;
    }

    setColor(color) {
        this.color = color;

    }

    setPosition_x(position_x) {
        this.position_x = position_x;

    }

    setPosition_y(position_y) {
        this.position_y = position_y;

    }

    setWidth(width) {
        this.width = width;

    }
    setHeight(height) {
        this.height = height;

    }

}